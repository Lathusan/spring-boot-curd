package com.JavaAssignment.transformer;

import org.springframework.stereotype.Component;

import com.JavaAssignment.domain.Product;
import com.JavaAssignment.dto.ProductDto;

@Component
public class ProductTransformer implements BaseTransformer<Product, ProductDto> {

	@Override
	public ProductDto transform(Product product) {
		ProductDto productDto = null;
		if (product != null) {
			productDto = new ProductDto();
			productDto.setId(product.getId());
			productDto.setName(product.getName());
			productDto.setBrand(product.getBrand());
			productDto.setPrice(product.getPrice());
			productDto.setIsActive(product.getIsActive());
		}
		return productDto;
	}

	@Override
	public Product reverseTransform(ProductDto productDto) {
		Product product = null;
		if (productDto != null) {
			product = new Product();
			product.setId(productDto.getId());
			product.setName(productDto.getName());
			product.setBrand(productDto.getBrand());
			product.setPrice(productDto.getPrice());
			product.setIsActive(productDto.getIsActive());
		}
		return product;
	}

}
