package com.JavaAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAssignmentBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaAssignmentBackendApplication.class, args);
	}

}
