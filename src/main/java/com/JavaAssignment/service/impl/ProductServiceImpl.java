package com.JavaAssignment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JavaAssignment.dto.ProductDto;
import com.JavaAssignment.dto.ResponseDto;
import com.JavaAssignment.service.ProductService;
import com.JavaAssignment.service.ServiceUtil;
import com.JavaAssignment.service.bl.ProductServiceBL;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	ServiceUtil serviceUtil;
	
	@Autowired
	private ProductServiceBL productServiceBL;
	
	@Override
	public ResponseDto getAll() {
		ResponseDto responseDto = null;
		List<ProductDto> productDtoList = productServiceBL.getAll();
		responseDto = serviceUtil.getServiceResponse(productDtoList);
		return responseDto;
	}

	@Override
	public ResponseDto save(ProductDto productDto) {
		ResponseDto responseDto = null;
		ProductDto saveProductDto = productServiceBL.save(productDto);
		responseDto = serviceUtil.getServiceResponse(saveProductDto);
		return responseDto;
	}

	@Override
	public ResponseDto updateStatus(Long productId, Boolean isActive) {
		ResponseDto responseDto = null;
		ProductDto saveProductDto = productServiceBL.updateStatus(productId, isActive);
		responseDto = serviceUtil.getServiceResponse(saveProductDto);
		return responseDto;
	}

}
