package com.JavaAssignment.dao;

import java.util.List;

import com.JavaAssignment.domain.Product;
import com.JavaAssignment.dto.ProductDto;

public interface ProductDao extends BaseDao<Product> {

	List<ProductDto> getAll();

	ProductDto save(ProductDto productDto);

	ProductDto updateStatus(ProductDto productDto);

	ProductDto updateStatus(Long productId, Boolean isActive);

}
