package com.JavaAssignment.service;

import com.JavaAssignment.dto.ProductDto;
import com.JavaAssignment.dto.ResponseDto;

public interface ProductService {
	
	public ResponseDto getAll();
	
	public ResponseDto save(ProductDto warrantyDto);

	public ResponseDto updateStatus(Long productId, Boolean isActive);

}
