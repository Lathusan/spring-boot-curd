package com.JavaAssignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.JavaAssignment.dto.ProductDto;
import com.JavaAssignment.dto.ResponseDto;
import com.JavaAssignment.service.ProductService;

@RestController
@RequestMapping("product")
public class ProductController {

	@Autowired
	ProductService productService;

	@GetMapping("/getAll")
	public ResponseDto getAll() {
		return productService.getAll();
	}

	@PostMapping("/save")
	public ResponseDto save(@RequestBody ProductDto productDto) {
		return productService.save(productDto);
	}

	@PutMapping("/update")
	public ResponseDto update(@RequestBody ProductDto productDto) {
		return productService.save(productDto);
	}

	@PutMapping("/updateStatus")
	public ResponseDto updateStatus(@RequestParam("productId") Long productId,
			@RequestParam("isActive") Boolean isActive) {
		return productService.updateStatus(productId, isActive);
	}

}
