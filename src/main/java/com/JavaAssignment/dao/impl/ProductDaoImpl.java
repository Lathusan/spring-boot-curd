package com.JavaAssignment.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.JavaAssignment.dao.ProductDao;
import com.JavaAssignment.domain.Product;
import com.JavaAssignment.dto.ProductDto;
import com.JavaAssignment.transformer.ProductTransformer;

@Repository
public class ProductDaoImpl extends BaseDaoImpl<Product> implements ProductDao {

	@Autowired
	ProductTransformer productTransformer;

	@Override
	public List<ProductDto> getAll() {
		Criteria criteria = getCurrentSession().createCriteria(Product.class, "Product");
		criteria.addOrder(Order.asc("Product.id"));
		List<Product> productList = criteria.list();
		List<ProductDto> productDtoList = null;

		if (productList != null && !productList.isEmpty()) {
			productDtoList = productList.stream().map(bankDto -> {
				return productTransformer.transform(bankDto);
			}).collect(Collectors.toList());
		}
		return productDtoList;
	}

	@Transactional
	@Override
	public ProductDto save(ProductDto productDto) {
		Product product = productTransformer.reverseTransform(productDto);
		Product saveProduct = saveOrUpdate(product);
		return productTransformer.transform(saveProduct);
	}

	@Transactional
	@Override
	public ProductDto updateStatus(ProductDto productDto) {
		Product product = productTransformer.reverseTransform(productDto);
		Product saveProduct = merge(product);
		return productTransformer.transform(saveProduct);
	}

	@Transactional
	@Override
	public ProductDto updateStatus(Long productId, Boolean isActive) {
		Criteria criteria = getCurrentSession().createCriteria(Product.class, "Product");
		criteria.add(Restrictions.eq("Product.id", productId));
		Product product = (Product) criteria.uniqueResult();
		ProductDto productDto = null;
		if (product != null) {
			productDto = productTransformer.transform(product);
		}
		return productDto;
	}

}
