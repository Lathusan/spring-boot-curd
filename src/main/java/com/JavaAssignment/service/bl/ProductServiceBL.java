package com.JavaAssignment.service.bl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JavaAssignment.dao.ProductDao;
import com.JavaAssignment.dto.ProductDto;

@Service
public class ProductServiceBL {
	
	@Autowired
	private ProductDao productDao;
	
	public List<ProductDto> getAll() {
		return productDao.getAll();
	}
	
	public ProductDto save(ProductDto productDto) {
		return productDao.save(productDto);
	}

	public ProductDto updateStatus(Long productId, Boolean isActive) {
		ProductDto productDto = productDao.updateStatus(productId, isActive);
		if (productDto != null) {
			productDto.setIsActive(isActive);
			return productDao.updateStatus(productDto);
		} else {
			return null;
		}
	}

}
